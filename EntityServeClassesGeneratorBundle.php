<?php

namespace Skoromnui\Bundle\EntityServeClassGeneratorBundle;

use Skoromnui\Bundle\EntityServeClassGeneratorBundle\Command\GenerateBuilderCommand;
use Skoromnui\Bundle\EntityServeClassGeneratorBundle\Command\GenerateControllerCommand;
use Skoromnui\Bundle\EntityServeClassGeneratorBundle\Command\GenerateDTOCommand;
use Skoromnui\Bundle\EntityServeClassGeneratorBundle\Command\GenerateEntityServeClassesCommand;
use Skoromnui\Bundle\EntityServeClassGeneratorBundle\Command\GenerateRepositoryCommand;
use Skoromnui\Bundle\EntityServeClassGeneratorBundle\Command\GenerateServiceCommand;
use Symfony\Component\Console\Application;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class EntityServeClassesGeneratorBundle extends Bundle
{
    /**
     * {@inheritDoc}
     */
    public function build(ContainerBuilder $container)
    {
        parent::build($container);
    }

    /**
     * {@inheritDoc}
     */
    public function registerCommands(Application $application)
    {
        $generateDTOCommand = new GenerateDTOCommand();
        $generateRepositoryCommand = new GenerateRepositoryCommand();
        $generateBuilderCommand = new GenerateBuilderCommand();
        $generateServiceCommand = new GenerateServiceCommand();
        $generateControllerCommand = new GenerateControllerCommand();

        $generateEntityServeClassesCommand = new GenerateEntityServeClassesCommand(
            $generateDTOCommand,
            $generateRepositoryCommand,
            $generateBuilderCommand,
            $generateServiceCommand,
            $generateControllerCommand
        );

        $application->addCommands([
            $generateDTOCommand,
            $generateRepositoryCommand,
            $generateBuilderCommand,
            $generateServiceCommand,
            $generateEntityServeClassesCommand,
            $generateControllerCommand
        ]);
    }
}
