## Symfony Entity Serve Class Generator

Package for generating serve classes (Repository, DTO, Builder, Service) for entity.

### Warning ##

This package only works with modular architecture.

for example entity namespace should be like this
**App\Modules\\_Module Name_\\Entity\\_Entity Name_** 

### Install package
```bash
composer require alexskoromnui/symfony-entity-serve-classes-generator --dev
```

Then you should register bundle inside bundles.php

``` 
    Skoromnui\Bundle\EntityServeClassGeneratorBundle\EntityServeClassesGeneratorBundle::class => ['dev' => true]

```

### Commands

- **bin/console s_generate:all** - command for generating (DTO, Repository, Builder, Service, Controller) based on Entity

- **bin/console s_generate:dto** - command for generating DTO and DTO Interface based on Entity
- **bin/console s_generate:repository** - command for generating Repository and Repository Interface based on Entity
- **bin/console s_generate:builder** - command for generating Builder based on Entity
- **bin/console s_generate:service** - command for generating Service based on Entity
- **bin/console s_generate:controller** - command for generating Controller based on Entity

