<?php

namespace Skoromnui\Bundle\EntityServeClassGeneratorBundle\Service;


class WordFormatConverted
{
    public static function camelCaseToSnakeCase(string $word): string
    {
        return strtolower(preg_replace('/(?<!^)[A-Z]/', '_$0', $word));
    }

    public static function camelCaseToDashSeparated(string $word): string
    {
        return strtolower(preg_replace('%([a-z])([A-Z])%', '\1-\2', $word));
    }
}