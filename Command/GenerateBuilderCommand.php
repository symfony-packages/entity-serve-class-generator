<?php

namespace Skoromnui\Bundle\EntityServeClassGeneratorBundle\Command;

use Nette\PhpGenerator\ClassType;
use Nette\PhpGenerator\PhpFile;
use Nette\PhpGenerator\PsrPrinter;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Style\SymfonyStyle;

class GenerateBuilderCommand extends Command
{
    protected static $defaultName = 's_generate:builder';

    protected function configure()
    {
        $this->setDescription('Command for generating entity builder');

    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $helper = $this->getHelper('question');

        $moduleNameQuestion = new Question('<info>Please enter the module name: </info>');
        $moduleName = $helper->ask($input, $output, $moduleNameQuestion);

        $entityNameQuestion = new Question('<info>Please enter the entity name: </info>');
        $entityName = $helper->ask($input, $output, $entityNameQuestion);

        $entityNamespace = 'App\Modules\\' . $moduleName . '\Entity\\';

        if (!class_exists($entityNamespace . $entityName)) {
            $io->error($entityName . ' does not exist in ' . $moduleName . ' module');
        }

        $this->generateBuilderForEntity($entityName, $moduleName, $io);

        return 0;
    }

    public function generateBuilderForEntity(
        string $entityName,
        string $moduleName,
        SymfonyStyle $io
    )
    {
        $entityClassReflection = new \ReflectionClass('App\Modules\\' . $moduleName . '\Entity\\' . $entityName);
        $entityClassProperties = $entityClassReflection->getProperties();

        $file = new PhpFile;
        $filePrinter = new PsrPrinter();

        $builderNamespace = $file->addNamespace('App\Modules\\' . $moduleName . '\\Builder');
        $builderNamespace->addUse('App\Modules\\' . $moduleName . '\Entity\\'. $entityName);
        $builderNamespace->addUse('App\Modules\\' . $moduleName . '\DTO\\'. $entityName . 'DTOInterface');

        $builderClassName = $entityName . 'Builder';
        $class = $builderNamespace->addClass($builderClassName);

        $this->createBuildMethod($class, $entityName, $moduleName);
        $this->createFillMethod($class, $entityName, $moduleName, $entityClassProperties);

        $fileNameWithPath = 'src/Modules/'. $moduleName . '/Builder/'.$builderClassName.'.php';

        file_put_contents($fileNameWithPath, $filePrinter->printFile($file));

        $io->success('Builder for ' . $entityName . ' successfully created!');
    }

    private function createBuildMethod(ClassType $class, string $entityName, string $moduleName)
    {
        $method = $class->addMethod('build')
            ->addBody('$entity = new '. ucwords($entityName).'();')
            ->addBody('$this->fill($entity, $dto);')
            ->addBody('')
            ->addBody('return $entity;');

        $method->addParameter('dto')
            ->setType('App\Modules\\' . $moduleName . '\DTO\\'. $entityName . 'DTOInterface');
        $method->setReturnType('App\Modules\\' . $moduleName . '\Entity\\'. $entityName);
    }

    /**
     * @param ClassType             $class
     * @param string                $entityName
     * @param \ReflectionProperty[] $entityClassProperties
     * @param string                $moduleName
     */
    private function createFillMethod(ClassType $class, string $entityName, string $moduleName, $entityClassProperties)
    {
        $body = '';

        foreach ($entityClassProperties as $classProperty) {
            $propertyName = $classProperty->getName();

            $body .= 'is_null($' . $propertyName .' = $dto->get'. ucwords($propertyName).'()) ?: $entity->set'. ucwords($propertyName).'($'. $propertyName .');' . "\n";
        }

        $method = $class->addMethod('fill')->setBody($body);
        $method->addParameter('entity')
            ->setType('App\Modules\\' . $moduleName . '\Entity\\'. $entityName);
        $method->addParameter('dto')
            ->setType('App\Modules\\' . $moduleName . '\DTO\\'. $entityName . 'DTOInterface');
    }
}
