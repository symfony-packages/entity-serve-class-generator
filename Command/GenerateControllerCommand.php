<?php

namespace Skoromnui\Bundle\EntityServeClassGeneratorBundle\Command;


use FOS\RestBundle\View\View;
use Nette\PhpGenerator\ClassType;
use Nette\PhpGenerator\PhpFile;
use Nette\PhpGenerator\PsrPrinter;
use Skoromnui\Bundle\EntityServeClassGeneratorBundle\Service\WordFormatConverted;
use Skoromnui\Bundle\EntityServeClassGeneratorBundle\Service\WordFormConverter;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class GenerateControllerCommand extends Command
{
    protected static $defaultName = 's_generate:controller';

    protected function configure()
    {
        $this->setDescription('Command for generating entity controller');

    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $helper = $this->getHelper('question');

        $moduleNameQuestion = new Question('<info>Please enter the module name: </info>');
        $moduleName = $helper->ask($input, $output, $moduleNameQuestion);

        $entityNameQuestion = new Question('<info>Please enter the entity name: </info>');
        $entityName = $helper->ask($input, $output, $entityNameQuestion);

        $entityNamespace = 'App\Modules\\' . $moduleName . '\Entity\\';

        if (!class_exists($entityNamespace . $entityName)) {
            $io->error($entityName . ' does not exist in ' . $moduleName . ' module');
        }

        $this->generateControllerForEntity($entityName, $moduleName, $io);

        return 0;
    }

    public function generateControllerForEntity(string $entityName, string $moduleName, SymfonyStyle $io)
    {
        $file = new PhpFile;
        $filePrinter = new PsrPrinter();

        $controllerNamespace = $file->addNamespace('App\Modules\\' . $moduleName . '\\Controller');
        $controllerNamespace->addUse('App\Modules\\' . $moduleName . '\\Service\\' . $entityName . 'Service');
        $controllerNamespace->addUse('App\Modules\\' . $moduleName . '\\DTO\\' . $entityName . 'Request');
        $controllerNamespace->addUse('App\Modules\\' . $moduleName . '\\Entity\\' . $entityName);
        $controllerNamespace->addUse('UrsusArctosUA\SearchHelper\Dictionary\Params');
        $controllerNamespace->addUse('UrsusArctosUA\SearchHelper\Finder');
        $controllerNamespace->addUse('App\Modules\Search\Arguments\SimpleSearch');
        $controllerNamespace->addUse(AuthorizationCheckerInterface::class);
        $controllerNamespace->addUse(AccessDeniedException::class);
        $controllerNamespace->addUse(Request::class);
        $controllerNamespace->addUse(View::class);
        $controllerNamespace->addUse(Route::class);
        $controllerNamespace->addUse(UrlGeneratorInterface::class);

        $controllerClassName = $entityName . 'Controller';
        $class = $controllerNamespace->addClass($controllerClassName);

        $snakeCaseEntityName = WordFormatConverted::camelCaseToSnakeCase($entityName);
        $dashSeparatedEntityName = WordFormatConverted::camelCaseToDashSeparated($entityName);

        $this->createConstructor($class, $entityName, $moduleName);
        $this->createCreateMethod($class, $entityName, $snakeCaseEntityName, $dashSeparatedEntityName);
        $this->createUpdateMethod($class, $entityName, $snakeCaseEntityName, $dashSeparatedEntityName);
        $this->createGetMethod($class, $entityName, $snakeCaseEntityName, $dashSeparatedEntityName);
        $this->createDeleteMethod($class, $entityName, $snakeCaseEntityName, $dashSeparatedEntityName);
        $this->createGetAllMethod($class, $entityName, $snakeCaseEntityName, $dashSeparatedEntityName);
        $this->createGetCountMethod($class, $entityName, $snakeCaseEntityName, $dashSeparatedEntityName);

        $fileNameWithPath = 'src/Modules/'. $moduleName . '/Controller/'. $controllerClassName.'.php';

        file_put_contents($fileNameWithPath, $filePrinter->printFile($file));

        $io->success('Controller for ' . $entityName . ' successfully created!');
    }

    private function createConstructor(ClassType $class, string $entityName, string $moduleName)
    {
        $serviceName = lcfirst($entityName) .'Service';

        $class->addProperty($serviceName)->setPrivate();
        $class->addProperty('security')->setPrivate();

        $constructor = $class->addMethod('__construct');
        $constructor->setPublic();
        $constructor->addBody('$this->'. $serviceName .' = $'. $serviceName .';');
        $constructor->addBody('$this->security = $security;');
        $constructor->addParameter($serviceName)
            ->setType('App\Modules\\' . $moduleName . '\\Service\\' . $entityName . 'Service');
        $constructor->addParameter('security')
            ->setType(AuthorizationCheckerInterface::class);
    }

    private function createCreateMethod(ClassType $class, string $entityName, string $snakeCaseEntityName, string $dashSeparatedEntityName)
    {
        $method = $class->addMethod('create');
        $method->addParameter('request')
            ->setType(Request::class);
        $method->addParameter('router')
            ->setType(UrlGeneratorInterface::class);
        $method->addBody('$dto = new '. $entityName . 'Request($request);')
            ->addBody('')
            ->addBody('try {')
            ->addBody('    $'. lcfirst($entityName) .' = $this->'. lcfirst($entityName) .'Service->create($dto);')
            ->addBody('} catch (\Exception $e) {')
            ->addBody('    return new View([\'message\' => $e->getMessage()], 500);')
            ->addBody('}')
            ->addBody('')
            ->addBody('$url = $router->generate(\'get_'. $snakeCaseEntityName .'\', [\'id\' => $'. lcfirst($entityName) .'->getId()]);')
            ->addBody('')
            ->addBody('return new View($'. lcfirst($entityName) .', 201, [\'Location\' => $url]);');

        $method->addComment('@param Request               $request');
        $method->addComment('@param UrlGeneratorInterface $router');
        $method->addComment('');
        $method->addComment('@Route("/'. WordFormConverter::pluralize($dashSeparatedEntityName) .'.{_format}", methods={"POST"}, name="create_'. $snakeCaseEntityName .'", ');
        $method->addComment('      defaults={"_format"=null})');
        $method->addComment('');
        $method->addComment('@return View');
    }

    private function createUpdateMethod(ClassType $class, string $entityName, string $snakeCaseEntityName, string $dashSeparatedEntityName)
    {
        $method = $class->addMethod('update');
        $method->addParameter('id')
            ->setType('int');
        $method->addParameter('request')
            ->setType(Request::class);
        $method->addParameter('router')
            ->setType(UrlGeneratorInterface::class);
        $method->addBody('$'. lcfirst($entityName) .' = $this->'. lcfirst($entityName) .'Service->get($id);')
            ->addBody('')
            ->addBody('$dto = new '. $entityName . 'Request($request);')
            ->addBody('')
            ->addBody('try {')
            ->addBody('    $'. lcfirst($entityName) .' = $this->'. lcfirst($entityName) .'Service->update($'. lcfirst($entityName) .', $dto);')
            ->addBody('} catch (\Exception $e) {')
            ->addBody('    return new View([\'message\' => $e->getMessage()], 500);')
            ->addBody('}')
            ->addBody('')
            ->addBody('$url = $router->generate(\'get_'. $snakeCaseEntityName .'\', [\'id\' => $'. lcfirst($entityName) .'->getId()]);')
            ->addBody('')
            ->addBody('return new View(null, 204, [\'Location\' => $url]);');

        $method->addComment('@param int                   $id')
            ->addComment('@param Request               $request')
            ->addComment('@param UrlGeneratorInterface $router')
            ->addComment('')
            ->addComment('@Route("/'. WordFormConverter::pluralize($dashSeparatedEntityName) .'/{id}.{_format}", methods={"PATCH"}, name="update_'. $snakeCaseEntityName .'", ')
            ->addComment('     defaults={"_format"=null}, requirements={"id"="\d+"})')
            ->addComment('')
            ->addComment('@return View');
    }

    private function createGetMethod(ClassType $class, string $entityName, string $snakeCaseEntityName, string $dashSeparatedEntityName)
    {
        $method = $class->addMethod('get')
            ->addBody('$'. lcfirst($entityName) .' = $this->'. lcfirst($entityName) .'Service->get($id);')
            ->addBody('')
            ->addBody('return new View($'. lcfirst($entityName) .', 200);');

        $method->addParameter('id')->setType('int');

        $method->addComment('@param int                   $id')
            ->addComment('')
            ->addComment('@Route("/'. WordFormConverter::pluralize($dashSeparatedEntityName) .'/{id}.{_format}", methods={"GET"}, name="get_'. $snakeCaseEntityName .'",')
            ->addComment('     defaults={"_format"=null}, requirements={"id"="\d+"})')
            ->addComment('')
            ->addComment('@return View');
    }

    private function createDeleteMethod(ClassType $class, string $entityName, string $snakeCaseEntityName, string $dashSeparatedEntityName)
    {
        $method = $class->addMethod('delete')
            ->addBody('$'. lcfirst($entityName) .' = $this->'. lcfirst($entityName) .'Service->get($id);')
            ->addBody('')
            ->addBody('$this->'. lcfirst($entityName) .'Service->delete($'. lcfirst($entityName)  .');')
            ->addBody('')
            ->addBody('return new View(null, 204);');

        $method->addParameter('id')->setType('int');

        $method->addComment('@param int                   $id')
            ->addComment('')
            ->addComment('@Route("/'. WordFormConverter::pluralize($dashSeparatedEntityName) .'/{id}.{_format}", methods={"DELETE"}, name="delete_'. $snakeCaseEntityName .'",')
            ->addComment('     defaults={"_format"=null}, requirements={"id"="\d+"})')
            ->addComment('')
            ->addComment('@return View');
    }

    private function createGetAllMethod(ClassType $class, string $entityName, string $snakeCaseEntityName, string $dashSeparatedEntityName)
    {
        $method = $class->addMethod('all')
            ->addBody('$filter = $request->get(\'filter\', []);')
            ->addBody('$order = $request->get(\'order_by\', [\'id\' => \'DESC\']);')
            ->addBody('')
            ->addBody('$'. WordFormConverter::pluralize(lcfirst($entityName)) .' = $searchHelper->search(')
            ->addBody('    '. $entityName . '::class,')
            ->addBody('    new Params($filter, $request->get(\'limit\', 20), $request->get(\'offset\', 0), $order)')
            ->addBody(');')
            ->addBody('')
            ->addBody('return new View($'. WordFormConverter::pluralize(lcfirst($entityName)) .', 200);');

        $method->addParameter('request')->setType(Request::class);
        $method->addParameter('searchHelper')->setType('UrsusArctosUA\SearchHelper\Finder');

        $method->addComment('@param Request                   $request')
            ->addComment('@param Finder                   $searchHelper')
            ->addComment('')
            ->addComment('@Route("/'. WordFormConverter::pluralize($dashSeparatedEntityName) .'.{_format}", methods={"GET"}, name="get_'. WordFormConverter::pluralize($snakeCaseEntityName) .'",')
            ->addComment('     defaults={"_format"=null})')
            ->addComment('')
            ->addComment('@return View');
    }

    private function createGetCountMethod(ClassType $class, string $entityName, string $snakeCaseEntityName, string $dashSeparatedEntityName)
    {
        $method = $class->addMethod('total')
            ->addBody('$count = $finder->count('. $entityName .'::class, $params);')
            ->addBody('')
            ->addBody('return new View($count, 200);');

        $method->addParameter('finder')->setType('UrsusArctosUA\SearchHelper\Finder');
        $method->addParameter('params')->setType('App\Modules\Search\Arguments\SimpleSearch');

        $method->addComment('@param Finder                   $finder')
            ->addComment('@param SimpleSearch                   $params')
            ->addComment('')
            ->addComment('@Route("/'. WordFormConverter::pluralize($dashSeparatedEntityName) .'/total.{_format}", methods={"GET"}, name="count_'. WordFormConverter::pluralize($snakeCaseEntityName) .'",')
            ->addComment('     defaults={"_format"=null})')
            ->addComment('')
            ->addComment('@return View');
    }
}
