<?php

namespace Skoromnui\Bundle\EntityServeClassGeneratorBundle\Command;


use Nette\PhpGenerator\ClassType;
use Nette\PhpGenerator\PhpFile;
use Nette\PhpGenerator\PsrPrinter;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Style\SymfonyStyle;

class GenerateEntityServeClassesCommand extends Command
{
    /**
     * @var GenerateDTOCommand
     */
    private $DTOCommand;
    /**
     * @var GenerateRepositoryCommand
     */
    private $repositoryCommand;
    /**
     * @var GenerateBuilderCommand
     */
    private $builderCommand;
    /**
     * @var GenerateServiceCommand
     */
    private $serviceCommand;
    /**
     * @var GenerateControllerCommand
     */
    private $controllerCommand;

    public function __construct(
        GenerateDTOCommand $DTOCommand,
        GenerateRepositoryCommand $repositoryCommand,
        GenerateBuilderCommand $builderCommand,
        GenerateServiceCommand $serviceCommand,
        GenerateControllerCommand $controllerCommand,
        $name = null
    )
    {
        parent::__construct($name);

        $this->DTOCommand = $DTOCommand;
        $this->repositoryCommand = $repositoryCommand;
        $this->builderCommand = $builderCommand;
        $this->serviceCommand = $serviceCommand;
        $this->controllerCommand = $controllerCommand;
    }

    protected static $defaultName = 's_generate:all';

    protected function configure()
    {
        $this->setDescription('Command for generating entity serve classes (DTO, Repository, Builder, Service');

    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $helper = $this->getHelper('question');

        $moduleNameQuestion = new Question('<info>Please enter the module name: </info>');
        $moduleName = $helper->ask($input, $output, $moduleNameQuestion);

        $entityNameQuestion = new Question('<info>Please enter the entity name: </info>');
        $entityName = $helper->ask($input, $output, $entityNameQuestion);

        $entityNamespace = 'App\Modules\\' . $moduleName . '\Entity\\';

        if (!class_exists($entityNamespace . $entityName)) {
            $io->error($entityName . ' does not exist in ' . $moduleName . ' module');
        }

        $question = new ConfirmationQuestion('<comment>Create dto for entity (y|yes)?: </comment> ', false,  '/^(y|yes)/i');

        if ($helper->ask($input, $output, $question)) {
            $this->DTOCommand->generateDTOForEntity($entityName, $moduleName, $io);
        }

        $question = new ConfirmationQuestion('<comment>Create repository for entity (y|yes)?:</comment> ', false,  '/^(y|yes)/i');

        if ($helper->ask($input, $output, $question)) {
            $this->repositoryCommand->generateRepositoryForEntity($entityName, $moduleName, $io);
        }

        $question = new ConfirmationQuestion('<comment>Create builder for entity (y|yes)?:</comment> ', false,  '/^(y|yes)/i');

        if ($helper->ask($input, $output, $question)) {
            $this->builderCommand->generateBuilderForEntity($entityName, $moduleName, $io);
        }

        $question = new ConfirmationQuestion('<comment>Create service for entity (y|yes)?:</comment> ', false,  '/^(y|yes)/i');

        if ($helper->ask($input, $output, $question)) {
            $this->serviceCommand->generateServiceForEntity($entityName, $moduleName, $io);
        }

        $question = new ConfirmationQuestion('<comment>Create controller for entity (y|yes)?:</comment> ', false,  '/^(y|yes)/i');

        if ($helper->ask($input, $output, $question)) {
            $this->controllerCommand->generateControllerForEntity($entityName, $moduleName, $io);
        }

        return 0;
    }

}
