<?php

namespace Skoromnui\Bundle\EntityServeClassGeneratorBundle\Command;


use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Nette\PhpGenerator\ClassType;
use Nette\PhpGenerator\PhpFile;
use Nette\PhpGenerator\PsrPrinter;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Style\SymfonyStyle;

class GenerateRepositoryCommand extends Command
{
    protected static $defaultName = 's_generate:repository';

    protected function configure()
    {
        $this->setDescription('Command for generating entity repository');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $helper = $this->getHelper('question');

        $moduleNameQuestion = new Question('<info>Please enter the module name: </info>');
        $moduleName = $helper->ask($input, $output, $moduleNameQuestion);

        $entityNameQuestion = new Question('<info>Please enter the entity name: </info>');
        $entityName = $helper->ask($input, $output, $entityNameQuestion);

        $entityNamespace = 'App\Modules\\' . $moduleName . '\Entity\\';

        if (!class_exists($entityNamespace . $entityName)) {
            $io->error($entityName . ' does not exist in ' . $moduleName . ' module');
        }

        $this->generateRepositoryForEntity($entityName, $moduleName, $io);

        return 0;
    }

    public function generateRepositoryForEntity(string $entityName, string $moduleName, SymfonyStyle $io)
    {
        $this->createRepositoryInterface($entityName, $moduleName);
        $this->createRepository($entityName, $moduleName);

        $io->success('Repository for ' . $entityName . ' successfully created!');
    }

    private function createRepositoryInterface(string $entityName, string $moduleName)
    {
        $file = new PhpFile;
        $filePrinter = new PsrPrinter();

        $repositoryInterfaceNamespace = $file->addNamespace('App\Modules\\' . $moduleName . '\\Repository');
        $repositoryInterfaceNamespace->addUse('App\Modules\\' . $moduleName . '\Entity\\' . $entityName);
        $repositoryInterfaceName = $entityName . 'RepositoryInterface';

        $interface = $repositoryInterfaceNamespace->addInterface($repositoryInterfaceName);
        $this->createInterfaceMethods($interface, $entityName, $moduleName);

        $fileNameWithPath = 'src/Modules/'. $moduleName . '/Repository/'.$repositoryInterfaceName.'.php';

        file_put_contents($fileNameWithPath, $filePrinter->printFile($file));
    }

    private function createInterfaceMethods(ClassType $class, string $entityName, string $moduleName)
    {
        $method = $class->addMethod('find')->setPublic();
        $method->addParameter('id')->setType('int');
        $method->addParameter('lockMode')->setDefaultValue(null);
        $method->addParameter('lockVersion')->setDefaultValue(null);
        $method->addComment('@return ' . $entityName . '|null');


        $method = $class->addMethod('findOneBy')->setPublic();
        $method->addParameter('criteria')->setType('array');
        $method->addParameter('orderBy')->setType('array')->setDefaultValue(null);
        $method->addComment('@return ' . $entityName . '|null');

        $method = $class->addMethod('findBy')->setPublic();
        $method->addParameter('criteria')->setType('array');
        $method->addParameter('orderBy')->setType('array')->setDefaultValue(null);
        $method->addParameter('limit')->setDefaultValue(null);
        $method->addParameter('offset')->setDefaultValue(null);
        $method->addComment('@return ' . $entityName . '[]');

        $method = $class->addMethod('save')->setPublic();
        $method->addParameter('entity')
            ->setType('App\Modules\\' . $moduleName . '\Entity\\' . $entityName);

        $method = $class->addMethod('remove')->setPublic();
        $method->addParameter('entity')
            ->setType('App\Modules\\' . $moduleName . '\Entity\\' . $entityName);
    }

    private function createRepository(string $entityName, string $moduleName)
    {
        $file = new PhpFile;
        $filePrinter = new PsrPrinter();

        $repositoryNamespace = $file->addNamespace('App\Modules\\' . $moduleName . '\\Repository');
        $repositoryNamespace->addUse('App\Modules\\' . $moduleName . '\Entity\\' . $entityName);
        $repositoryNamespace->addUse(ServiceEntityRepository::class);
        $repositoryNamespace->addUse(ManagerRegistry::class);
        $repositoryName = $entityName . 'Repository';

        $class = $repositoryNamespace->addClass($repositoryName);
        $class->addExtend(ServiceEntityRepository::class);
        $class->addImplement('App\Modules\\' . $moduleName . '\\Repository\\' . $entityName . 'RepositoryInterface');
        $class->addComment('@method '. $entityName.'|null find($id, $lockMode = null, $lockVersion = null)');
        $class->addComment('@method '. $entityName .'|null findOneBy(array $criteria, array $orderBy = null)');
        $class->addComment('@method '. $entityName .'[]    findAll()');
        $class->addComment('@method '. $entityName .'[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)');

        $this->createRepositoryMethods($class, $entityName, $moduleName);

        $fileNameWithPath = 'src/Modules/'. $moduleName . '/Repository/'.$repositoryName.'.php';

        file_put_contents($fileNameWithPath, $filePrinter->printFile($file));
    }

    private function createRepositoryMethods(ClassType $class, string $entityName, string $moduleName)
    {
        $constructor = $class->addMethod('__construct');
        $constructor->setPublic();
        $constructor->addBody('parent::__construct($registry, '. $entityName .'::class);');
        $constructor->addParameter('registry')->setType(ManagerRegistry::class);

        $method = $class->addMethod('save')->setPublic();
        $method->addBody('$this->_em->persist($entity);');
        $method->addBody('$this->_em->flush();');
        $method->addParameter('entity')
            ->setType('App\Modules\\' . $moduleName . '\Entity\\' . $entityName);

        $method = $class->addMethod('remove')->setPublic();
        $method->addBody('$this->_em->remove($entity);');
        $method->addBody('$this->_em->flush();');
        $method->addParameter('entity')
            ->setType('App\Modules\\' . $moduleName . '\Entity\\' . $entityName);
    }
}
