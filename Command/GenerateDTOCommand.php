<?php

namespace Skoromnui\Bundle\EntityServeClassGeneratorBundle\Command;

use Nette\PhpGenerator\ClassType;
use Nette\PhpGenerator\PhpFile;
use Nette\PhpGenerator\PsrPrinter;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\HttpFoundation\Request;

class GenerateDTOCommand extends Command
{
    protected static $defaultName = 's_generate:dto';

    protected function configure()
    {
        $this->setDescription('Command for generating entity dto');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $helper = $this->getHelper('question');

        $moduleNameQuestion = new Question('<info>Please enter the module name: </info>');
        $moduleName = $helper->ask($input, $output, $moduleNameQuestion);

        $entityNameQuestion = new Question('<info>Please enter the entity name: </info>');
        $entityName = $helper->ask($input, $output, $entityNameQuestion);

        $entityNamespace = 'App\Modules\\' . $moduleName . '\Entity\\';

        if (!class_exists($entityNamespace . $entityName)) {
            $io->error($entityName . ' does not exist in ' . $moduleName . ' module');
        }

        $this->generateDTOForEntity($entityName, $moduleName, $io);

        return 0;
    }

    public function generateDTOForEntity(string $entityName, string $moduleName, SymfonyStyle $io)
    {
        $entityClassReflection = new \ReflectionClass('App\Modules\\' . $moduleName . '\Entity\\' . $entityName);
        $entityClassProperties = $entityClassReflection->getProperties();

        $this->createDTOInterface($entityName, $moduleName, $entityClassProperties);
        $this->createRequestDTO($entityName, $moduleName, $entityClassProperties);

        $io->success('DTO for ' . $entityName . ' successfully created!');
    }

    /**
     * @param string       $entityName
     * @param string       $moduleName
     * @param \ReflectionProperty[]  $entityClassProperties
     */
    private function createDTOInterface(
        string $entityName,
        string $moduleName,
        $entityClassProperties
    )
    {
        $file = new PhpFile;
        $filePrinter = new PsrPrinter();

        $dtoNamespace = $file->addNamespace('App\Modules\\' . $moduleName . '\\DTO');
        $dtoInterfaceName = $entityName . 'DTOInterface';

        $interface = $dtoNamespace->addInterface($dtoInterfaceName);

        $this->createInterfaceMethods($interface, $entityClassProperties);

        $fileNameWithPath = 'src/Modules/'. $moduleName . '/DTO/'.$dtoInterfaceName.'.php';

        file_put_contents($fileNameWithPath, $filePrinter->printFile($file));
    }

    /**
     * @param ClassType             $class
     * @param \ReflectionProperty[] $entityClassProperties
     */
    private function createInterfaceMethods(ClassType $class, $entityClassProperties)
    {
        foreach ($entityClassProperties as $classProperty) {
            $propertyName = $classProperty->getName();

            $class->addMethod('get' . ucwords($propertyName))->setPublic()->setReturnNullable();
        }
    }

    /**
     * @param string       $entityName
     * @param string       $moduleName
     * @param \ReflectionProperty[]  $entityClassProperties
     */
    private function createRequestDTO(
        string $entityName,
        string $moduleName,
        $entityClassProperties
    )
    {
        $file = new PhpFile;
        $filePrinter = new PsrPrinter();

        $dtoNamespace = $file->addNamespace('App\Modules\\' . $moduleName . '\\DTO');
        $dtoNamespace->addUse(Request::class);
        $dtoName = $entityName . 'Request';

        $class = $dtoNamespace->addClass($dtoName);
        $class->addImplement('App\Modules\\' . $moduleName . '\\DTO\\' . $entityName . 'DTOInterface');
        $class->addProperty('request')->setPrivate();

        $this->createRequestDTOMethods($class, $entityClassProperties);

        $fileNameWithPath = 'src/Modules/'. $moduleName . '/DTO/'.$dtoName.'.php';

        file_put_contents($fileNameWithPath, $filePrinter->printFile($file));
    }

    /**
     * @param ClassType             $class
     * @param \ReflectionProperty[] $entityClassProperties
     */
    private function createRequestDTOMethods(ClassType $class, $entityClassProperties)
    {
        $constructor = $class->addMethod('__construct');
        $constructor->setPublic();
        $constructor->setBody('$this->request = $request;');
        $constructor->addParameter('request')->setType(Request::class);

        foreach ($entityClassProperties as $classProperty) {
            $propertyName = $classProperty->getName();
            $requestProperty = $output = strtolower(preg_replace('/(?<!^)[A-Z]/', '_$0', $propertyName));

            $method = $class->addMethod('get' . ucwords($propertyName))->setPublic();
            $method->setBody('return $this->request->get(\''. $requestProperty.'\');');
        }
    }
}
