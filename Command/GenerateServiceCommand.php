<?php

namespace Skoromnui\Bundle\EntityServeClassGeneratorBundle\Command;


use Nette\PhpGenerator\ClassType;
use Nette\PhpGenerator\PhpFile;
use Nette\PhpGenerator\PsrPrinter;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class GenerateServiceCommand extends Command
{
    protected static $defaultName = 's_generate:service';

    protected function configure()
    {
        $this->setDescription('Command for generating entity service');

    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $helper = $this->getHelper('question');

        $moduleNameQuestion = new Question('<info>Please enter the module name: </info>');
        $moduleName = $helper->ask($input, $output, $moduleNameQuestion);

        $entityNameQuestion = new Question('<info>Please enter the entity name: </info>');
        $entityName = $helper->ask($input, $output, $entityNameQuestion);

        $entityNamespace = 'App\Modules\\' . $moduleName . '\Entity\\';

        if (!class_exists($entityNamespace . $entityName)) {
            $io->error($entityName . ' does not exist in ' . $moduleName . ' module');
        }

        $this->generateServiceForEntity($entityName, $moduleName, $io);

        return 0;
    }

    public function generateServiceForEntity(string $entityName, string $moduleName, SymfonyStyle $io)
    {
        $file = new PhpFile;
        $filePrinter = new PsrPrinter();

        $serviceNamespace = $file->addNamespace('App\Modules\\' . $moduleName . '\\Service');
        $serviceNamespace->addUse('App\Modules\\' . $moduleName . '\\Entity\\' . $entityName);
        $serviceNamespace->addUse('App\Modules\\' . $moduleName . '\\DTO\\' . $entityName . 'DTOInterface');
        $serviceNamespace->addUse('App\Modules\\' . $moduleName . '\\Repository\\' . $entityName . 'RepositoryInterface');
        $serviceNamespace->addUse('App\Modules\\' . $moduleName . '\\Builder\\' . $entityName . 'Builder');
        $serviceNamespace->addUse(ValidatorInterface::class);

        $serviceClassName = $entityName . 'Service';
        $class = $serviceNamespace->addClass($serviceClassName);

        $this->createConstructor($class, $entityName, $moduleName);
        $this->createCreateMethod($class, $entityName, $moduleName);
        $this->createUpdateMethod($class, $entityName, $moduleName);
        $this->createGetMethod($class, $entityName, $moduleName);
        $this->createDeleteMethod($class, $entityName, $moduleName);

        $fileNameWithPath = 'src/Modules/'. $moduleName . '/Service/'.$serviceClassName.'.php';

        file_put_contents($fileNameWithPath, $filePrinter->printFile($file));

        $io->success('Service for ' . $entityName . ' successfully created!');
    }

    private function createConstructor(ClassType $class, string $entityName, string $moduleName)
    {
        $class->addProperty('repository')->setPrivate();
        $class->addProperty('builder')->setPrivate();
        $class->addProperty('validator')->setPrivate();

        $constructor = $class->addMethod('__construct');
        $constructor->setPublic();
        $constructor->addBody('$this->repository = $repository;');
        $constructor->addBody('$this->builder = $builder;');
        $constructor->addBody('$this->validator = $validator;');
        $constructor->addParameter('repository')
            ->setType('App\Modules\\' . $moduleName . '\\Repository\\' . $entityName . 'RepositoryInterface');
        $constructor->addParameter('builder')
            ->setType('App\Modules\\' . $moduleName . '\\Builder\\' . $entityName . 'Builder');
        $constructor->addParameter('validator')
            ->setType(ValidatorInterface::class);
    }

    private function createCreateMethod(ClassType $class, string $entityName, string $moduleName)
    {
        $method = $class->addMethod('create')
            ->addBody('$entity = $this->builder->build($dto);')
            ->addBody('')
            ->addBody('$errors = $this->validator->validate($entity);')
            ->addBody('')
            ->addBody('if (count($errors) > 0) {')
            ->addBody('    throw new \Exception((string) $errors);')
            ->addBody('}')
            ->addBody('')
            ->addBody('$this->repository->save($entity);')
            ->addBody('')
            ->addBody('return $entity;');
        $method->setReturnType('App\Modules\\' . $moduleName . '\\Entity\\' . $entityName)
            ->setReturnNullable();


        $method->addParameter('dto')->setType('App\Modules\\' . $moduleName . '\\DTO\\' . $entityName . 'DTOInterface');
    }

    private function createUpdateMethod(ClassType $class, string $entityName, string $moduleName)
    {
        $method = $class->addMethod('update')
            ->addBody('$this->builder->fill($entity, $dto);')
            ->addBody('')
            ->addBody('$errors = $this->validator->validate($entity);')
            ->addBody('')
            ->addBody('if (count($errors) > 0) {')
            ->addBody('    throw new \Exception((string) $errors);')
            ->addBody('}')
            ->addBody('')
            ->addBody('$this->repository->save($entity);')
            ->addBody('')
            ->addBody('return $entity;');
        $method->setReturnType('App\Modules\\' . $moduleName . '\\Entity\\' . $entityName)
            ->setReturnNullable();

        $method->addParameter('entity')
            ->setType('App\Modules\\' . $moduleName . '\\Entity\\' . $entityName);
        $method->addParameter('dto')
            ->setType('App\Modules\\' . $moduleName . '\\DTO\\' . $entityName . 'DTOInterface');
    }

    private function createGetMethod(ClassType $class, string $entityName, string $moduleName)
    {
        $method = $class->addMethod('get')
            ->addBody('return $this->repository->find($id);');

        $method->addParameter('id')->setType('int');
        $method->setReturnType('App\Modules\\' . $moduleName . '\\Entity\\' . $entityName)
            ->setReturnNullable();
    }

    private function createDeleteMethod(ClassType $class, string $entityName, string $moduleName)
    {
        $method = $class->addMethod('delete')
            ->addBody('return $this->repository->remove($entity);');

        $method->addParameter('entity')
            ->setType('App\Modules\\' . $moduleName . '\\Entity\\' . $entityName);
    }
}
